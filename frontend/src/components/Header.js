import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div className="text-center">
        <img
          src="https://assets.themuse.com/uploaded/companies/12211/small_logo.png?v=35507b2cd80a88df0a9c4a10f0047a51711053751bd7635ac80bd5788eb33e7d"
          width="300"
          className="img-thumbnail"
          style={{ marginTop: "20px" }}
          alt="new"
        />
        <hr />
        <h5>
          <i>presents</i>
        </h5>
        <h1>App with React + Django</h1>
      </div>
    );
  }
}

export default Header;
