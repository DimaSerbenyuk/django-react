let API_SERVER_VAL = '';

switch (process.env.NODE_ENV) {
    case 'development':
        API_SERVER_VAL = 'http://localhost:8000/api/students/';
        break;
    case 'production':
        API_SERVER_VAL = process.env.REACT_APP_API_SERVER;
        break;
    default:
        API_SERVER_VAL = 'http://localhost:8000/api/students/';
        break;
}

//export const API_URL = "http://localhost:8000/api/students/";
export const API_URL = API_SERVER_VAL;

export const SESSION_DURATION = 5*3600*1000;