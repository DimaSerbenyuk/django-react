import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('http://localhost:3000/');
  await page.getByRole('button', { name: 'Create New' }).click();
  await page.locator('input[name="name"]').click();
  await page.locator('input[name="name"]').fill('dima');
  await page.locator('input[name="email"]').click();
  await page.locator('input[name="email"]').fill('dserbeniuk@gmail.com');
  await page.locator('input[name="document"]').click();
  await page.locator('input[name="document"]').fill('new');
  await page.locator('input[name="phone"]').click();
  await page.locator('input[name="phone"]').fill('0964325678');
  await page.getByRole('button', { name: 'Send' }).click();
  await page.getByRole('button', { name: 'Close' }).click();
});