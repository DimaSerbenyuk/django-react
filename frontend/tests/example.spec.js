// @ts-check
const { test, expect } = require('@playwright/test');

test('homepage has title and links to intro page', async ({ page }) => {
  await page.goto('https://playwright.dev/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Playwright/);

  // create a locator
  const getStarted = page.getByRole('link', { name: 'Get started' });

  // Expect an attribute "to be strictly equal" to the value.
  await expect(getStarted).toHaveAttribute('href', '/docs/intro');

  // Click the get started link.
  await getStarted.click();
  
  // Expects the URL to contain intro.
  await expect(page).toHaveURL(/.*intro/);
});
// import { test, expect } from '@playwright/test';

// test('test', async ({ page }) => {
//   await page.goto('http://localhost:3000/');
//   await page.getByRole('button', { name: 'Create New' }).click();
//   await page.locator('input[name="name"]').click();
//   await page.locator('input[name="name"]').fill('dima');
//   await page.locator('input[name="email"]').click();
//   await page.locator('input[name="email"]').fill('d@gmail.com');
//   await page.locator('input[name="document"]').click();
//   await page.locator('input[name="document"]').fill('dde');
//   await page.locator('input[name="phone"]').click();
//   await page.locator('input[name="phone"]').fill('098434234242');
//   await page.getByRole('button', { name: 'Send' }).click();
// });
