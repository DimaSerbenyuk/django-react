#!/bin/sh
# if [ "$DATABASE" = "postgres" ]
# then
#     echo "Waiting for postgres..."
    
#     while ! nc -z $DB_HOST $DB_PORT; do
#       sleep 0.1
#     done
    
#     echo "PostgreSQL started"
# fi

# cd backend

# python manage.py collectstatic --noinput
# python manage.py migrate --noinput
# echo "from django.contrib.auth.models import User;
# User.objects.filter(email='$DJANGO_ADMIN_EMAIL').delete();
# User.objects.create_superuser('dima', 'dserbenyukgood@gmail.com', 'dima121212')" | python manage.py shell

# exec "$@"


until cd /backend
do
    echo "Waiting for server volume..."
done

until ./manage.py makemigrations
do
    echo "Waiting for db to be ready..."
    sleep 2
done

until ./manage.py migrate --run-syncdb
do
    echo "ready..."
done

./manage.py collectstatic --noinput

#gunicorn backend.wsgi --bind 0.0.0.0:8000 --workers 4 --threads 4
#./manage.py runserver 0.0.0.0:8003 manage.py migrate --run-syncdb